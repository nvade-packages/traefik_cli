## Add any function here that is needed in more than one parts of your
## application, or that you otherwise wish to extract from the main function
## scripts.
##
## Note that code here should be wrapped inside bash functions, and it is
## recommended to have a separate file for each function.
##
## Subdirectories will also be scanned for *.sh, so you have no reason not
## to organize your code neatly.
##
function determine_os() {
  if [[ "$OSTYPE" == "linux-gnu"* ]]; then
    echo "linux"
  elif [[ "$OSTYPE" == "darwin"* ]]; then
    echo "mac"
  fi
}

OS=$(determine_os)
if [[ "$OS" == "mac" ]]; then
  ORANGE='%c[38;5;172m'
  GREEN='%c[38;5;113m'
  BLINK='%c[5;m'
  YELLOW="%c[1;33m"
  RED='%c[31m'
  NC='%c[0m'
elif [[ "$OS" == "linux" ]]; then
  ORANGE='\e[38;5;172m'
  GREEN='\e[38;5;113m'
  BLINK='\e[5;m'
  YELLOW="\e[1;33m"
  RED='\e[31m'
  NC='\e[0m'
fi

function newline() {
  echo ""
}

function msg() {
  echo -e "$1 $NC $(newline)"
}

function info() {
  msg "$YELLOW $1"
}

function danger() {
  msg "$RED $1"
}

function success() {
  msg "$GREEN $1"
}
