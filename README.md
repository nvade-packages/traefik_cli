# Traefik Local Domain Setup

## To install
```
/bin/bash -c "$(curl -fsSL https://gitlab.onlinq.systems/niels/traefik/-/blob/master/install)"
```

## Using traefik
```shell
# Start traefik
traefik up

# Stop traefik
traefik down

# Update traefik
traefik update

# Uninstall traefik
traefik uninstall
```
